=== Bundle Product Manager ===
Contributors: alexlavigin
Tags: wordpress woocommerce, woocommerce plugin, product bundles, additional products, order customization, customer experience, sales enhancement, ecommerce solution, custom product sets
Requires at least: 6.0
Tested up to: 6.3.1
Requires PHP: 7.4
Stable tag: 1.0.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Our WordPress WooCommerce plugin provides unique functionality by allowing you to easily add multiple additional products to your main product before checkout.

== Description ==

Our WordPress WooCommerce plugin provides unique functionality by allowing you to easily add multiple
additional products to your main product before checkout. This is a convenient solution for customers who want to
collect complex sets of products and easily customize their orders. Improve the customer experience and increase
sales with our plugin.

**Important!** Bundle Product Manager plugin requires an active WooCommerce plugin.
